import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'

Vue.use(VueMaterial)


var url = "https://fantasy.premierleague.com/drf/bootstrap-static";

export default {
    name: 'app',
    data: function () {
      return {
        igralci: []
      }
    },
    mounted() { // when the Vue app is booted up, this is run automatically.
    var self = this // create a closure to access component in the callback below
    $.getJSON(url, function(data) {
      self.igralci = data.elements;
    });
}